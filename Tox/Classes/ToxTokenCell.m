//
//  ToxCell.m
//  Tox
//
//  Created by Florian Schwehn on 09.03.13.
//  Copyright (c) 2013 fschwehnSOFT. All rights reserved.
//

#import "ToxTokenCell.h"
#import "ToxToken.h"

#define V_MARGIN 2.0
#define H_MARGIN 1.0
#define H_PADDING 8.0

@interface ToxTokenCell()
{
    NSSize size;
    NSTimeInterval lastClickTime;
}
@end

@implementation ToxTokenCell

- (id)initWithToken:(ToxToken *)token
{
    self = [super init];
    if (self) {
        self.token = token;
    }
    return self;
}

+ (id)tokenCellWithToken:(ToxToken *)token
{
    return [[ToxTokenCell alloc] initWithToken:token];
}

#pragma mark - accessors

- (NSSize)cellSize
{
    return size;
}

- (NSPoint)cellBaselineOffset
{
    return NSMakePoint(0, -6);
}

- (void)setToken:(ToxToken *)token
{
    _token = token;
    self.stringValue = token.displayString;
    size = [token.displayString sizeWithAttributes:@{NSFontAttributeName: self.font}];
    size.width += 2.0 * H_MARGIN + 2.0 * H_PADDING;
    size.height += 2.0 * V_MARGIN;
}

#pragma mark - drawing

- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView
{
    NSRect innerFrame = cellFrame;
    innerFrame.origin.x += H_MARGIN;
    innerFrame.origin.y += V_MARGIN;
    innerFrame.size.width -= 2.0 * H_MARGIN;
    innerFrame.size.height -= 2.0 * V_MARGIN;
    
    // draw background
    NSBezierPath *p = [NSBezierPath bezierPathWithRoundedRect:innerFrame xRadius:8 yRadius:8];
    [[NSColor darkGrayColor] setFill];
    [p fill];
    
    // draw text
    NSPoint textPos = { innerFrame.origin.x + H_PADDING, innerFrame.origin.y - 1 };
    NSDictionary *attributes = @{NSFontAttributeName: self.font, NSForegroundColorAttributeName: [NSColor whiteColor]};
    [self.stringValue drawAtPoint:textPos withAttributes:attributes];
}

#pragma mark - events

- (BOOL)trackMouse:(NSEvent *)theEvent inRect:(NSRect)cellFrame ofView:(NSView *)controlView untilMouseUp:(BOOL)flag
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(tokenCellProcessDoubleClick:)]) {
        // handle double click
        NSTimeInterval clickTime = CFAbsoluteTimeGetCurrent();
        NSTimeInterval clickInterval = clickTime - lastClickTime;
        lastClickTime = clickTime;
        
        if (clickInterval < 1.0 && clickInterval > 0.0) {
            if ([self.delegate tokenCellProcessDoubleClick:self])
                return YES;
        }
    }
    
    return [super trackMouse:theEvent inRect:cellFrame ofView:controlView untilMouseUp:flag];
}

@end

//
//  ToxToken.h
//  Tox
//
//  Created by Florian Schwehn on 11.03.13.
//  Copyright (c) 2013 fschwehnSOFT. All rights reserved.
//

#import "ToxTextView.h"

@interface ToxToken : NSObject

@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *displayString;

+ (id)tokenWithIdentifier:(NSString *)identifier;
+ (id)tokenWithIdentifier:(NSString *)identifier displayString:(NSString *)displayString;
+ (NSString *)tokenIdentifierPattern;
+ (NSRegularExpression *)tokenIdentifierRegularExpression;

- (id)initWithIdentifier:(NSString *)identifier displayString:(NSString *)displayString;

@end

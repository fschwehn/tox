//
//  ToxTextView.m
//  Tox
//
//  Created by Florian Schwehn on 10.03.13.
//  Copyright (c) 2013 fschwehnSOFT. All rights reserved.
//

#import "ToxTextView.h"
#import "ToxTokenCell.h"
#import "ToxToken.h"

@interface ToxTextView()
{
    NSRegularExpression *tokenPatternExpression;
}
@end

@implementation ToxTextView

#pragma mark - initialization

- (void)initialize
{
    self.tokenPrefix = @"<#";
    self.tokenPostfix = @"#>";
}

- (id)init
{
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self initialize];
    }
    return self;
}

#pragma mark - paste borad behaviour

- (NSArray *)writablePasteboardTypes
{
    return @[NSStringPboardType];
}

- (NSArray *)readablePasteboardTypes
{
    return @[NSStringPboardType];
}

- (BOOL)writeSelectionToPasteboard:(NSPasteboard *)pboard type:(NSString *)type
{
    if ([type isEqualToString:NSStringPboardType]) {
        [pboard setString:[self encodeStringInRange:self.selectedRange] forType:type];
        return YES;
    }
    
    return [super writeSelectionToPasteboard:pboard type:type];
}

- (BOOL)readSelectionFromPasteboard:(NSPasteboard *)pboard type:(NSString *)type
{
    if ([type isEqualToString:NSStringPboardType]) {
        [self insertText:[self decodeString:[pboard stringForType:type]]];
        return YES;
    }
    return [super readSelectionFromPasteboard:pboard type:type];
}

#pragma mark - accessors

- (void)setTokenPrefix:(NSString *)prefix postFix:(NSString *)postFix
{
    self.tokenPrefix = prefix;
    self.tokenPostfix = postFix;
}

- (NSString *)string
{
    return [self encodedString];
}

- (void)setString:(NSString *)string
{
    [self setEncodedString:string];
}

- (void)setEncodedString:(NSString *)string
{
    [self selectAll:nil];
    [self insertText:[self decodeString:string]];
}

- (NSString *)encodedString
{
    return [self encodeStringInRange:NSMakeRange(0, super.string.length)];
}


#pragma mark - editing

- (void)insertToken:(ToxToken *)token
{
    if (self.tokenPool[token.identifier] == token) {
        [self insertText:[self attributedStringWithToken:token]];
    }
}

#pragma mark - coding

- (NSAttributedString *)decodeString:(NSString *)string
{
    NSString *pattern = [NSString stringWithFormat:@"%@%@%@",
                         [NSRegularExpression escapedPatternForString:self.tokenPrefix],
                         [ToxToken tokenIdentifierPattern],
                         [NSRegularExpression escapedPatternForString:self.tokenPostfix]];
    NSError *err;
    NSRegularExpression *rx = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&err];
    assert(!err);
    NSArray *matches = [rx matchesInString:string options:0 range:NSMakeRange(0, string.length)];
    
    if (matches.count) {
        NSMutableAttributedString *buffer = [NSMutableAttributedString new];
        
        // extract preceding string
        NSInteger firstMatchLocation = [matches[0] range].location;
        
        if (firstMatchLocation > 0) {
            [buffer appendAttributedString:[[NSAttributedString alloc] initWithString:[string substringWithRange:NSMakeRange(0, firstMatchLocation)]]];
        }
        
        for (NSInteger i = 0; i < matches.count; i++) {
            // extract token
            NSTextCheckingResult *match = matches[i];
            NSString *tokenIdentifier = [string substringWithRange:NSMakeRange(match.range.location + 2, match.range.length - 4)];
            ToxToken *token = self.tokenPool[tokenIdentifier];
            [buffer appendAttributedString:token ?
             [self attributedStringWithToken:token] :
             [[NSAttributedString alloc] initWithString:[string substringWithRange:match.range]]];
            
            // extract succeding string
            NSInteger location = match.range.location + match.range.length;
            
            if (location < string.length) {
                NSInteger length = (i == matches.count - 1 ? string.length : [matches[i+1] range].location) - location;
                [buffer appendAttributedString:[[NSAttributedString alloc] initWithString:[string substringWithRange:NSMakeRange(location, length)]]];
            }
        }
        
        return buffer;
    }
    
    return [[NSAttributedString alloc] initWithString:string];
}

- (NSString *)encodeStringInRange:(NSRange)range
{
    NSString *string = [self.textStorage.string substringWithRange:range];
    NSCharacterSet *attachmentCharacterSet = [NSCharacterSet characterSetWithRange:NSMakeRange(NSAttachmentCharacter, 1)];
    NSScanner *scanner = [NSScanner scannerWithString:string];
    scanner.charactersToBeSkipped = nil;
    NSMutableString *buffer = [NSMutableString new];
    NSInteger currentTextLocation = 0;
    
    do {
        [scanner scanUpToCharactersFromSet:attachmentCharacterSet intoString:nil];
        
        NSInteger scanLocation = scanner.scanLocation;
        NSInteger length = scanLocation - currentTextLocation;
        NSRange textRange = NSMakeRange(currentTextLocation, length);
        NSString *text = [string substringWithRange:textRange];
        [buffer appendString:text];
        
        if ([scanner isAtEnd]) {
            break;
        }
        else {
            NSTextAttachment *attachment = [self.textStorage attribute:NSAttachmentAttributeName atIndex:(range.location + scanLocation) effectiveRange:nil];
            
            if ([attachment.attachmentCell isKindOfClass:ToxTokenCell.class]) {
                [buffer appendFormat:@"%@%@%@", self.tokenPrefix, ((ToxTokenCell *)attachment.attachmentCell).token.identifier, self.tokenPostfix];
            }
            
            currentTextLocation = scanLocation + 1;
            [scanner setScanLocation:currentTextLocation];
        }
    } while (![scanner isAtEnd]);
    
    return buffer;
}

#pragma mark - private

- (NSAttributedString *)attributedStringWithToken:(ToxToken *)token
{
    NSTextAttachment *attachment = [NSTextAttachment new];
    ToxTokenCell *cell = [[ToxTokenCell alloc] initWithToken:token];
    cell.delegate = self;
    [attachment setAttachmentCell:cell];
    return [NSAttributedString attributedStringWithAttachment:attachment];
}

@end

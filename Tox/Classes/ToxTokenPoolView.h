//
//  ToxPoolView.h
//  Tox
//
//  Created by Florian Schwehn on 10.03.13.
//  Copyright (c) 2013 fschwehnSOFT. All rights reserved.
//

#import "ToxTextView.h"

@class ToxTokenCell;

@protocol ToxTokenPoolViewDelegate;

// ------------------------------------------------
@interface ToxTokenPoolView : ToxTextView
// ------------------------------------------------
@property (atomic, assign) id<ToxTokenPoolViewDelegate> tokenPoolViewDelegate;

@end

// ------------------------------------------------
@protocol ToxTokenPoolViewDelegate <NSObject>
// ------------------------------------------------
@required

- (BOOL)tokenPoolView:(ToxTokenPoolView *)tokenCell processDoubleClick:(ToxTokenCell *)cell;

@end

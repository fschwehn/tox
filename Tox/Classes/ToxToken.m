//
//  ToxToken.m
//  Tox
//
//  Created by Florian Schwehn on 11.03.13.
//  Copyright (c) 2013 fschwehnSOFT. All rights reserved.
//

#import "ToxToken.h"

@implementation ToxToken

+ (id)tokenWithIdentifier:(NSString *)identifier
{
    return [[ToxToken alloc] initWithIdentifier:identifier];
}

+ (id)tokenWithIdentifier:(NSString *)identifier displayString:(NSString *)displayString
{
    return [[ToxToken alloc] initWithIdentifier:identifier displayString:displayString];
}

- (id)initWithIdentifier:(NSString *)identifier
{
    self = [super init];
    if (self) {
        self.identifier = identifier;
    }
    return self;
}

- (id)initWithIdentifier:(NSString *)identifier displayString:(NSString *)displayString
{
    self = [self initWithIdentifier:identifier];
    if (self) {
        self.displayString = displayString;
    }
    return self;
}

#pragma mark - accessors

+ (NSString *)tokenIdentifierPattern
{
    return [ToxToken tokenIdentifierRegularExpression].pattern;
}

+ (NSRegularExpression *)tokenIdentifierRegularExpression
{
    static NSRegularExpression *rx;
    if (!rx) {
        NSError *err;
        rx = [NSRegularExpression regularExpressionWithPattern:@"[a-zA-Z_][.a-zA-Z_0-9]*" options:0 error:&err];
        assert(!err);
    }
    return rx;
}

- (void)setIdentifier:(NSString *)identifier
{
    // validate
    NSRange range = NSMakeRange(0, identifier.length);
    NSRange checkRange = [[ToxToken tokenIdentifierRegularExpression] rangeOfFirstMatchInString:identifier options:0 range:range];
    if (checkRange.location || checkRange.length != range.length) {
        NSLog(@"Invalid token identifier '%@'", identifier);
        _identifier = @"";
    }
    _identifier = identifier;
}

- (NSString *)displayString
{
    return _displayString ? _displayString : self.identifier;
}

@end

//
//  ToxController.m
//  Tox
//
//  Created by Florian Schwehn on 09.03.13.
//  Copyright (c) 2013 fschwehnSOFT. All rights reserved.
//

#import "ToxController.h"
#import "ToxTextView.h"
#import "ToxTokenPoolView.h"
#import "ToxTokenCell.h"
#import "ToxToken.h"

@interface ToxController ()

@end

@implementation ToxController

- (void)awakeFromNib
{
    self.poolView.tokenPoolViewDelegate = self;
    
    NSString *prefix = @"[#";
    NSString *postfix = @"#]";
    
    [self.editor setTokenPrefix:prefix postFix:postfix];
    [self.poolView setTokenPrefix:prefix postFix:postfix];
}

- (void)setTokenPool:(NSDictionary *)tokenPool
{
    _tokenPool = tokenPool;
    
    self.editor.tokenPool = tokenPool;
    self.poolView.tokenPool = tokenPool;
}

- (void)setTokenPoolWithArray:(NSArray *)tokens
{
    NSMutableDictionary *d = [[NSMutableDictionary alloc] initWithCapacity:tokens.count];
    for (ToxToken* t in tokens) {
        d[t.identifier] = t;
    }
    self.tokenPool = d;
}

#pragma mark - ToxTokenPoolViewDelegate

- (BOOL)tokenPoolView:(ToxTokenPoolView *)tokenCell processDoubleClick:(ToxTokenCell *)cell
{
    [self.editor insertToken:cell.token];
    return YES;
}

@end

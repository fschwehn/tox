//
//  ToxController.h
//  Tox
//
//  Created by Florian Schwehn on 09.03.13.
//  Copyright (c) 2013 fschwehnSOFT. All rights reserved.
//

#import "ToxTokenPoolView.h"

@interface ToxController : NSViewController <NSTextViewDelegate, ToxTokenPoolViewDelegate>

@property (nonatomic, assign) IBOutlet ToxTextView *editor;
@property (nonatomic, assign) IBOutlet ToxTokenPoolView *poolView;
@property (nonatomic, strong) NSDictionary *tokenPool;

- (void)setTokenPoolWithArray:(NSArray *)tokens;

@end

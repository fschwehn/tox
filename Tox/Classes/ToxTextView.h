//
//  ToxTextView.h
//  Tox
//
//  Created by Florian Schwehn on 10.03.13.
//  Copyright (c) 2013 fschwehnSOFT. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ToxTokenCell.h"

@interface ToxTextView : NSTextView<ToxTokenCellDelegate>

@property (nonatomic, strong) NSString *encodedString;
@property (nonatomic, strong) NSString *tokenPrefix;
@property (nonatomic, strong) NSString *tokenPostfix;
@property (nonatomic, strong) NSDictionary *tokenPool;

- (void)setTokenPrefix:(NSString *)prefix postFix:(NSString *)postFix;
- (void)insertToken:(ToxToken *)token;

@end

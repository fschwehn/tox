//
//  ToxCell.h
//  Tox
//
//  Created by Florian Schwehn on 09.03.13.
//  Copyright (c) 2013 fschwehnSOFT. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class ToxToken;

@protocol ToxTokenCellDelegate;

// ------------------------------------------------
@interface ToxTokenCell : NSTextAttachmentCell
// ------------------------------------------------

@property (nonatomic, strong) ToxToken *token;
@property (nonatomic, assign) id<ToxTokenCellDelegate> delegate;

+ (id)tokenCellWithToken:(ToxToken *)token;

- (id)initWithToken:(ToxToken *)token;

@end

// ------------------------------------------------
@protocol ToxTokenCellDelegate <NSObject>
// ------------------------------------------------

@optional

- (BOOL)tokenCellProcessDoubleClick:(ToxTokenCell *)cell;

@end

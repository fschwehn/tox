//
//  ToxPoolView.m
//  Tox
//
//  Created by Florian Schwehn on 10.03.13.
//  Copyright (c) 2013 fschwehnSOFT. All rights reserved.
//

#import "ToxTokenPoolView.h"

@implementation ToxTokenPoolView

- (void)awakeFromNib
{
    [self setEditable:NO];
}

- (BOOL)canBecomeKeyView
{
    return NO;
}

- (void)setTokenPool:(NSDictionary *)tokenPool
{
    [super setTokenPool:tokenPool];
    
    [self setEditable:YES];
    [self setString:@""];
    [self setEditable:NO];
    
    NSArray *sortedTokens = [tokenPool.allValues sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"displayString" ascending:YES]]];
    
    for (ToxToken *t in sortedTokens) {
        [self insertToken:t];
    }
}

- (void)insertToken:(ToxToken *)token
{
    [self setEditable:YES];
    [super insertToken:token];
    [self setEditable:NO];
}

#pragma mark - ToxTokenCellDelegate

- (BOOL)tokenCellProcessDoubleClick:(ToxTokenCell *)cell
{
    if (self.tokenPoolViewDelegate) {
        return [(id<ToxTokenPoolViewDelegate>)self.tokenPoolViewDelegate tokenPoolView:self processDoubleClick:cell];
    }
    return NO;
}

@end

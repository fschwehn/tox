//
//  AppDelegate.h
//  Tox
//
//  Created by Florian Schwehn on 08.03.13.
//  Copyright (c) 2013 fschwehnSOFT. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>

@class TextTemplateViewController;

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end

//
//  main.m
//  Tox
//
//  Created by Florian Schwehn on 08.03.13.
//  Copyright (c) 2013 fschwehnSOFT. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
